/**
 * Copyright (C) 2019 Intergral Information Solutions GmbH. All Rights Reserved
 */
package com.nerdvision.agent.reflect.impl;

import com.nerdvision.agent.reflect.api.IReflection;
import java.lang.reflect.Field;

/**
 *
 * @author nwightma
 */
public class ReflectionImpl9 implements IReflection
{

    @Override
    public boolean setAccessible( final Class clazz, final Field field )
    {
        try
        {
            final Module m = clazz.getModule();
            if( m.isNamed() )
            {
                final String pkgName = clazz.getPackageName();
                m.addOpens( pkgName, this.getClass().getModule() );
            }

            field.setAccessible( true );

            return true;
        }
        catch( final Exception e )
        {
            return false;
        }
    }

}
